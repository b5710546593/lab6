import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 * To test word counter.
 * @author Voraton Lertrattanapaisal
 *
 */
public class WordCount {
	/**
	 * Main for test.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[]args) throws IOException{
		String FILE_URL = "https://gist.githubusercontent.com/anonymous/0f0374e431bb88f403c9/raw/b8ad310a887093bca6bcc54192677c24bb00a7a2/AliceInWonderland.txt";
		URL url;
		url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		final String DELIMS = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter(DELIMS);
		WordCounter count = new WordCounter();
		while (scanner.hasNext()){
			count.addWord(scanner.next());
		}
		String[] wordList = count.getSortedWords();
		for (int i=0;i<20;i++){
			System.out.println(wordList[i]+" : "+count.getCount(wordList[i]));
		}
	}
}
