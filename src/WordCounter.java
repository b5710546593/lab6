import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Word Counter for count the word in paragraph or some text.
 * @author Voraton Lertrattanapaisal
 *
 */
public class WordCounter {
	/** Map for Store word and how many word? */
	HashMap<String, Integer> wordMap;
	/**
	 * To initial Word Counter.
	 */
	public WordCounter(){
		wordMap = new HashMap<String,Integer>();
	}
	/**
	 * To add word to word counter.
	 * @param word to add.
	 */
	public void addWord(String word){
		if (wordMap.get(word.toLowerCase())==null) wordMap.put(word.toLowerCase(), 1);
		else wordMap.put(word.toLowerCase(), (int)wordMap.get(word.toLowerCase())+1);
	}
	/**
	 * To get set of word.
	 * @return set of word.
	 */
	public Set<String> getWords(){
		return new HashSet<String>(wordMap.keySet());
	}
	/**
	 * To count the word.
	 * @param word to count.
	 * @return how many does word have.
	 */
	public int getCount(String word){
		return wordMap.get(word);
	}
	/**
	 * To get array of String from map
	 * @return array of String
	 */
	public String[] getSortedWords(){
		ArrayList wordList = new ArrayList(wordMap.keySet());
		Collections.sort(wordList, String.CASE_INSENSITIVE_ORDER);
		String[] wordArray = new String[wordList.size()];
		wordArray = (String[]) wordList.toArray(wordArray);
		return wordArray;
	}
}
